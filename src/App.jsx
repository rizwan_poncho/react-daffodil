// import { useState } from "react";
import "./App.css";
import Comp1 from "./components/comp1";

const App = () => {
  const data = {
    name: "Hamza",
    age: "23",
    city: "Dhaka",
  };
  // const [count, setCount] = useState(0);

  return (
    <>
      <div className="grid place-items-center py-12">
        <Comp1 myData={data} />
      </div>
      <p className="sazal">Bangladesh</p>
    </>
  );
};

export default App;
