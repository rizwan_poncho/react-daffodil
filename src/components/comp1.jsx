const Comp1 = (props) => {
  const city = "Dhaka";

  const h1Style = {
    color: "red",
    fontSize: "30px",
  };

  return (
    <div
      style={{
        width: "600px",
        textAlign: "center",
        border: "1px solid #ccc",
        padding: "20px",
      }}
      className="text-3xl"
    >
      My Name is {props.myData.name} <br /> My Age is {props.myData.age} <br />I
      live in {props.myData.city}
      <hr />
      <h1 style={h1Style}>Hello World</h1>
    </div>
  );
};

export default Comp1;
