import { Outlet } from "react-router-dom";
import Navbar from "./components/header";
import Footer from "./components/Footer";

const Layout = () => {
  return (
    <div>
      <Navbar />
      {/* <nav>
        <ul>
          <li>
            <a href="">HOME</a>
          </li>
          <li>
            <a href="">ABOUT</a>
          </li>
          <li>
            <a href="">SERVICE</a>
          </li>
          <li>
            <a href="">CONTACT</a>
          </li>
        </ul>
      </nav> */}

      <Outlet />

      <Footer />
    </div>
  );
};

export default Layout;
