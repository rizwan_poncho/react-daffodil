import Hero from "../components/Hero";
import OurTeam from "../components/OurTeam";

const Home = () => {
  return (
    <div>
      <Hero />
      <OurTeam />
    </div>
  );
};

export default Home;
